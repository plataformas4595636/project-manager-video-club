const express= require('express');

function create (req, res, next) {
    res.send('Permits create');
}

function list (req, res, next) {
    res.send('Permits list');
  }

function index (req, res, next) {
    res.send('Permits index');
}

function replace (req, res, next) {
    res.send('Permits replace');
}

function update(req, res, next) {
    res.send('Permits update');
}

function destroy (req, res, next) {
    res.send('Permits destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};