const express= require('express');

function create (req, res, next) {
    res.send('Social Medias create');
}

function list (req, res, next) {
    res.send('Social Medias list');
  }

function index (req, res, next) {
    res.send('Social Medias index');
}

function replace (req, res, next) {
    res.send('Social Medias replace');
}

function update(req, res, next) {
    res.send('Social Medias update');
}

function destroy (req, res, next) {
    res.send('Social Medias destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};