# Proyecto Reto 1: Desarrollar un manejador de proyectos

## Diagrama de clases:

![Diagrama de Clases](assets/Diagrama_de_Clases.jpeg)

- Clase User
  - Es el usuario que ingresa al sistema, puede ser cualquier miembro del equipo con cualquier rol.

- Método Login()
  - Aquí nos permite que todos los usuarios inicien sesión utilizando al menos tres redes sociales diferentes.

- Enum RoleType
  - Es el tipo de rol que se tiene en el proyecto.

- Enum SocialMedia
  - Aquí el usuario puede utilizar al menos 3 redes sociales diferentes y son las 3 redes sociales para ingresar al sistema.

- Clase embebida Role
  - Asegura que cada miembro del equipo tenga un propósito específico en el proyecto y solo pueda realizar ciertas acciones autorizadas.

- Clase Permit
  - Aquí es cuando el miembro del equipo solo puede realizar ciertas acciones autorizadas.

- Clase embebida Adress
  - Es la dirección de los miembros del equipo.

- Clase embebida Skill
  - Son las habilidades del miembro del equipo de desarrollo.

- Clase Developer
  - La empresa necesita tener información detallada sobre cada miembro del equipo de desarrollo y nos permite llevar un registro completo de los datos personales.

- Clase ProjectFile
  - La empresa necesita llevar un registro o expediente para cada proyecto de desarrollo y así mantener un registro organizado y completo de cada proyecto de desarrollo.

- Clase UserStory
  - En esta clase las historias tienen estos datos: narrativa de la historia (rol, característica, beneficio, prioridad y tiempo estimado), criterios de aceptación (contexto, evento y resultados).

- Enum Level
  - Nos dice que la clase Level se clasifica en tres niveles diferentes según su experiencia y las capacidades que tiene un usuario en habilidades específicas.

- Clase ReleaseBacklog
  - Será la posible entrega o entrega de una lista de funcionalidades o requisitos del proyecto funcional al Product Owner. Aquí la clase se organiza en tres columnas principales: Product Backlog, Release Backlog y Sprint Backlog.

- Clase Dashboard
  - Es el tablero donde podemos ver todo el desarrollo del proyecto visualmente.

- Clase ProductBacklog
  - Lista de todas las funcionalidades, mejoras, requisitos y demás elementos necesarios para el desarrollo del proyecto.

- Clase SprintBacklog
  - Sprint Backlog: Es la lista de funcionalidades y requisitos seleccionados del Product Backlog para ser completadas durante un sprint.

- Clase Evento
  - Describe la tarea o tareas que serán parte del UserStory.

- Clase Result
  - Describe los resultados que serán parte del UserStory.

## Diagrama de interacción:

![Diagrama de Interacción](assets/Diagrama_de_Interacción.png)

- Flujo (user -> sistema -> Rol): 
  - Todos los usuarios del sistema deben poder iniciar sesión en el sistema por medio de mínimo tres redes sociales.
  - Cada usuario cumple un rol dentro de un proyecto con diferentes permisos.

- Flujo (sistema -> tarjetas):
  - Explica el flujo de interacción entre los objetos con el dashboard y cómo dependiendo del rol se pueden hacer distintas actividades.

## Imagen de docker (Dockerhub):
https://hub.docker.com/repository/docker/billhaunter74/project_manager-master/general

## Authors
1. Brayan Ricardo Carrete Martínez - 307746


